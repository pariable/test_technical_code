const express = require('express')
const app = express()
const port = 4000
const path = require('path');
const bodyParser = require('body-parser');

app.use(express.static('assets'));
app.set('/assets', path.join(__dirname, '/assets'));

//allowed request data format
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.post("/segitiga",(req,res)=>{
    return res.status(200).json({
        status:true,
        message:"Data Ok",
        data:segitiga(req.body.data)
    })
})

app.post("/ganjil",(req,res)=>{
    return res.status(200).json({
        status:true,
        message:"Data Ok",
        data:ganjil(req.body.data)
    })
})

app.post("/prima",(req,res)=>{
    return res.status(200).json({
        status:true,
        message:"Data Ok",
        data:prima(req.body.data)
    })
})

//FUNCTION//
const segitiga=(p)=> {
    let h = '';
    let arr=[1,3,4,5]
    
    for (let i = 0; i < p; i++) {
        for (let j = 0; j <= i; j++) {
          if((j+1)==arr[j-0]  ){
            if(typeof arr[i] !="undefined"){
              h+=arr[i]
          
            }else{
              h +='0'; 
            }
            
          }

          h +='0'; 
        }
        
        h += '<br>';
    }
    return h;
}

const ganjil=(max)=>{
    var n="";
    var delimiter=","
    for(var i=0;i < max;i++){
        if(i % 2 !=0 ){
            delimiter=(max-1)==i?"":","
            n+=i+ delimiter;
        }
    }

    return n;
}

const prima=(angka)=>{
    let pembagi = 0;
    for(let i=1; i <= angka; i++){
        if(angka % i == 0){
        pembagi++
        }
    }
    if(pembagi == 2){
        console.log("prima")
        return pembagi;
    }
    return pembagi;
}



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})